import React from "react";
import {ITodoListItem} from "./ITodoListItem";
import TodoListItem from "./TodoListItem";

interface IProps {
    todoList: ITodoListItem[];
}

const TodoList: React.FC<IProps> = ({todoList}) => {
    return (
        <div className="list-group">
            {todoList.map((item, index) => {
                return (
                    <TodoListItem
                        index={index}
                        complete={item.complete}
                        date={item.date}
                        title={item.title}
                        id={item.id}
                        key={item.id}
                    />
                )
            })}
        </div>
    )
}

export default TodoList;
