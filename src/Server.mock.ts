import {Server} from "miragejs";
import {ITodoListItem} from "./ITodoListItem";

export default () => new Server({
    routes() {
        this.namespace = "/api";

        this.get("/todo-list", async (): Promise<ITodoListItem[]> => new Promise((resolve => {
            import("faker")
                .then((module) => {
                    const faker = module.default;

                    resolve(Array.from({length: 5}, (): ITodoListItem => {
                        return {
                            title: faker.random.words(2),
                            complete: faker.random.boolean(),
                            date: faker.date.future(),
                            id: faker.random.uuid(),
                        }
                    }))
                })
        })))
    }
})
