import React, {FormEvent, useState} from "react";
import {ITodoListItem} from "./ITodoListItem";

interface IProps {
    onSubmit: (item: ITodoListItem) => void;
}

const AddTodoItem: React.FC<IProps> = ({onSubmit}) => {
    const [value, setValue] = useState<string>("");

    const onFormSubmit = (e: FormEvent): void => {
        e.preventDefault();

        if (value.trim()) {
            import("faker")
                .then((module) => {
                    onSubmit({
                        complete: false,
                        title: value,
                        date: new Date(),
                        id: module.default.random.uuid(),
                    });

                    setValue("");
                })
        }
    }

    return (
        <form className="form-inline" onSubmit={onFormSubmit}>
            <div className="form-group mr-2 mb-2">
                <label htmlFor="staticEmail2" className="sr-only">Добавить</label>
                <input
                    type="text"
                    className="form-control"
                    id="staticEmail2"
                    value={value}
                    onChange={(e) => setValue(e.target.value)}
                />
            </div>
            <button type="submit" className="btn btn-primary mb-2">Add</button>
        </form>
    );
};

export default AddTodoItem;
