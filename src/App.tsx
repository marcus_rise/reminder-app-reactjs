import React, {useEffect, useState} from 'react';
import {ITodoListItem} from "./ITodoListItem";
import TodoList from "./TodoList";
import {Context, RemoveItem, SetDate, ToggleItem} from "./Context";
import AddTodoItem from "./AddTodoItem";

function App() {
    const [todoList, setTodoList] = useState<ITodoListItem[]>([]);

    const loadTodoList = (): void => {
        fetch('/api/todo-list')
            .then(response => response.json())
            .then((json: ITodoListItem[]) => {
                setTodoList(json.map(i => {
                    i.date = new Date(i.date);
                    return i;
                }))
            })
    };

    const toggleItem: ToggleItem = (id): void => {
        setTodoList(todoList.map((i) => {
            if (i.id === id) {
                i.complete = !i.complete;
            }

            return i;
        }))
    }

    const setDate: SetDate = (id, date): void => {
        setTodoList(todoList.map((i) => {
            if (i.id === id) {
                i.date = date ?? new Date();
            }

            return i;
        }))
    }

    const removeItem: RemoveItem = (id): void => {
        setTodoList(todoList.filter((i) => i.id !== id))
    }

    const addItem = (item: ITodoListItem): void => {
        setTodoList(todoList.concat(item));
    }

    useEffect(() => {
        loadTodoList();
    }, []);

    return (
        <Context.Provider value={{removeItem, setDate, toggleItem}}>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h1>Reminder</h1>
                        <hr/>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <AddTodoItem onSubmit={addItem}/>

                        <TodoList
                            todoList={todoList}
                        />
                    </div>
                </div>
            </div>
        </Context.Provider>
    );
}

export default App;
