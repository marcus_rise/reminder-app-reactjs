import React, {useContext, useEffect, useState} from "react";
import "./TodoListItem.scss"
import ReactDatePicker, {registerLocale, setDefaultLocale} from "react-datepicker";
import ru from 'date-fns/locale/ru';
import "react-datepicker/dist/react-datepicker.css";
import {Context} from "./Context";

registerLocale("ru", ru);
setDefaultLocale("ru")

interface IProps {
    title: string;
    complete: boolean;
    id: string;
    date: Date;
    index: number;
}

const TodoListItem: React.FC<IProps> = ({title, complete, id, date, index}) => {
    const [itemClassList, setItemClassList] = useState<string[]>([
        "list-group-item", "list-group-item-action", "todo-list-item"
    ]);
    const itemLabelClassList: string[] = ["form-check-label"];
    const {removeItem, setDate, toggleItem} = useContext(Context);

    if (complete) {
        itemLabelClassList.push("strike");
    }

    const remove = () => {
        setItemClassList(itemClassList.filter(i => i !== "ready").concat("remove"));

        setTimeout(() => {
            removeItem(id);
        }, 300);
    };

    useEffect(() => {
        setTimeout(() => {
            setItemClassList(c => c.concat("ready"));
        });
    }, []);

    return (
        <div className={itemClassList.join(" ")}>
            <div className="todo-list-item__toggle" onClick={() => toggleItem(id)}>
                <strong className="mr-2">{index + 1}. </strong>
                <div className="form-check form-check-inline">
                    <input
                        type="checkbox"
                        className="form-check-input"
                        id={`customCheck${id}`}
                        checked={complete}
                        onChange={() => toggleItem(id)}
                    />
                    <label
                        className={itemLabelClassList.join(" ")}
                        htmlFor={`customCheck${id}`}
                    >{title}</label>
                </div>
            </div>

            <ReactDatePicker
                className="form-control"
                selected={date}
                onChange={(e) => setDate(id, e)}
                showTimeSelect
                dateFormat={"d MMM yyyy HH:mm"}
                minDate={new Date()}
            />

            <button
                type="button"
                className="close ml-3"
                aria-label="Close"
                onClick={remove}
            >
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    )
};

export default TodoListItem;
