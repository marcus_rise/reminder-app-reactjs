export interface ITodoListItem {
    title: string;
    complete: boolean;
    id: string;
    date: Date
}
