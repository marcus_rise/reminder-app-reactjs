import React from "react";

export const Context = React.createContext<{
    toggleItem: ToggleItem,
    setDate: SetDate,
    removeItem: RemoveItem,
}>({
    removeItem: id => {
    },
    toggleItem: id => {
    },
    setDate: (id, date) => {
    },
});

export type ToggleItem = (id: string) => void;

export type SetDate = (id: string, date: Date | null) => void;

export type RemoveItem = (id: string) => void;
