const path = require("path");
const fs = require("fs");
const glob = require("glob-all");

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

const PurgeCssPlugin = require("purgecss-webpack-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
    webpack: {
        plugins: [
            new PurgeCssPlugin({
                paths: [
                    resolveApp("public/index.html"),
                    ...glob.sync(`${resolveApp("src")}/**/**/*`, {nodir: true})
                ]
            }),
            ...process.env.NODE_ENV === "production" ? [
                new BundleAnalyzerPlugin({
                    analyzerMode: "static",
                    // reportFilename: "/static/report.html",
                    // openAnalyzer: false,
                }),
            ] : [],
        ]
    }
};
